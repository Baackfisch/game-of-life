package de.baackfisch;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class GameScreen implements Screen {
    private SpriteBatch batch;
    private Texture cell;
    private boolean[][] cellsAlive;
    private boolean[][] cellsAliveNextTick;
    private float tickRate = 0.5f;
    private float time;
    private int often;

    private int cellSize = 20;

    public void create(){
        Pixmap c = new Pixmap(1,1,Pixmap.Format.RGB888);
        c.setColor(Color.GOLD);
        c.fill();
        cell = new Texture(c);

        batch = new SpriteBatch();

        time = 0;

        cellsAlive = new boolean[Gdx.graphics.getWidth() / cellSize][Gdx.graphics.getHeight() / cellSize];
        for(int x = 0; x < cellsAlive.length; x++){
            for (int y = 0; y < cellsAlive[x].length; y++){
                cellsAlive[x][y] = Math.random() < 0.3f;
            }
        }
        System.out.println(cellsAlive.length);
        System.out.println(cellsAlive[0].length);

        cellsAliveNextTick = new boolean[cellsAlive.length][cellsAlive[0].length];
        often = 0;
    }
    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        time+=delta;
        if(time > tickRate){
            if(checkAlive() == 0){
                for(int x = 0; x < cellsAlive.length; x++){
                    for (int y = 0; y < cellsAlive[x].length; y++){
                        cellsAlive[x][y] = Math.random() < 0.05f;
                    }
                }


                System.out.println(often++);
            }
            cellsAlive = cellsAliveNextTick.clone();
            time--;
        }
        batch.begin();
        for(int x = 0; x < cellsAlive.length; x++){
            for (int y = 0; y < cellsAlive[x].length; y++){
                if(cellsAlive[x][y]) batch.draw(cell,x * cellSize, y * cellSize, cellSize,cellSize);
            }
        }
        batch.end();
    }

    private int checkAlive(){
        int changes = 0;
        int corner = 0;
        int border = 0;
        int mid = 0;
        for(int x = 0; x < cellsAlive.length; x++){
            for (int y = 0; y < cellsAlive[x].length; y++){
                int cellsNearbyAlive = 0;
                int test = 0;
                //  X00
                //  X00
                //  X00
                if(x > 0){
                    cellsNearbyAlive += cellsAlive[x - 1][y] ? 1 : 0;
                    test++;
                    if(y > 0){
                        cellsNearbyAlive += cellsAlive[x - 1][y - 1] ? 1 : 0;
                        test++;
                    }
                    if(y < cellsAlive[x].length - 1){
                        cellsNearbyAlive += cellsAlive[x - 1][y + 1] ? 1 : 0;
                        test++;
                    }
                }
                //  00X
                //  00X
                //  00X
                if(x < cellsAlive.length - 1){
                    cellsNearbyAlive += cellsAlive[x + 1][y] ? 1 : 0;
                    test++;
                    if(y > 0){
                        cellsNearbyAlive += cellsAlive[x + 1][y - 1] ? 1 : 0;
                        test++;
                    }
                    if(y < cellsAlive[x].length - 1){
                        cellsNearbyAlive+= cellsAlive[x + 1][y + 1] ? 1 : 0;
                        test++;
                    }
                }

                //  0X0
                //  000
                //  000
                if(y > 0){
                    cellsNearbyAlive += cellsAlive[x][y - 1] ? 1 : 0;
                    test++;
                }
                //  000
                //  000
                //  0X0
                if(y < cellsAlive[x].length- 1){
                    cellsNearbyAlive += cellsAlive[x][y + 1] ? 1 : 0;
                    test++;
                }

                if(cellsAlive[x][y]){
                    if(cellsNearbyAlive == 2 || cellsNearbyAlive == 3){
                        cellsAliveNextTick[x][y] = true;
                    }else{
                        cellsAliveNextTick[x][y] = false;
                        changes++;
                    }
                }else{
                    if(cellsNearbyAlive == 3){
                        cellsAliveNextTick[x][y] = true;
                        changes++;
                    }

                }
                if(test == 3){
                    corner++;
                }
                if(test == 5){
                    border++;
                    //System.out.println(cellsNearbyAlive);
                }if(test == 8){
                    mid++;
                }
            }
        }
        return changes;
    }
    @Override
    public void resize(int width, int height) {

    }








    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
